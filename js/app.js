//Теория
//Я понял асинхронность, как ситуацию когда какое-либо действие выполняется вместе с другим действием с определенной разницей во времени. Всё.

//Задание
const root = document.querySelector('.root');
const taskURL1 = 'https://api.ipify.org/?format=json';
const taskURL2 = 'http://ip-api.com/';
const taskURL2Options = '?fields=continent,country,regionName,city,district';

async function getResponse(url) {
    let request = await fetch(url);
    let response = await request.json(); // (3)
    return response;
}

async function getIP(urlIP, urlInfo, options) {
    let getIP = await getResponse(urlIP);
    let getInfo = await getResponse(urlInfo + 'json/' + getIP.ip + options);
    return getInfo;
}

async function displayInfo(urlIP, urlInfo, options) {
    let arrData = await getIP(urlIP, urlInfo, options);

    for (let key in arrData) {
        if(!arrData[key]) {
            arrData[key] = 'Информацию не найдено!';
        }
        let p = document.createElement('p');
        p.textContent = `${key}: ${arrData[key]}`;
        root.append(p);
    }

}

document.querySelector('.btn').addEventListener('click', (evt) => {
    evt.preventDefault();
    if(root.classList.contains('checked')) {
        root.classList.remove('checked');
        let remP = document.querySelectorAll('p');
        for (const i of remP) {
            i.remove();
        }
    }
    root.classList.add('checked');
    displayInfo(taskURL1, taskURL2, taskURL2Options);
});
